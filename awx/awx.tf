provider "google" {
    credentials = "${file("keys/key.json")}"
    project = "${var.PROJECT}"
    region = "${var.REGION}"
}
data "template_file" "inventory" {
  template = "${file("./inventory.tpl")}"
  vars = {
    PASSWORD = "${var.PASSWORD}"
  }
}
resource "google_compute_instance" "awx" {
    name = "awx"
    machine_type = "n1-standard-2"
    zone = "${var.ZONE}"
    labels = {
        goal = "cm"
    }
    tags = ["http-server"]
    boot_disk {
        auto_delete = true
        device_name = "awx"
        mode = "READ_WRITE"
        initialize_params {
            size = 30
            type = "pd-standard"
            image = "ubuntu-os-cloud/ubuntu-minimal-1804-lts"
        }
    }
    network_interface {
        network = "default"
        access_config {}
    }
    scheduling {
        preemptible = true
        automatic_restart = false
    }
    description = "VM for running AWX server"
    metadata = {
        ssh-keys = "${var.REMOTE_USER}:${file("~/.ssh/id_rsa.pub")}"
    }
    provisioner "file" {
        content = "${data.template_file.inventory.rendered}"
        destination = "/tmp/inventory"
        connection {
            type = "ssh"
            user = "${var.REMOTE_USER}"
            host = "${self.network_interface.0.access_config.0.nat_ip}"
            private_key = "${file("~/.ssh/id_rsa")}"
        }
    }
    provisioner "remote-exec" {
        inline = [
            "sudo groupadd docker",
            "sudo usermod -aG docker $USER",
            "sudo apt -y update;sudo apt -y upgrade;",
            "echo 'deb http://ppa.launchpad.net/ansible/ansible/ubuntu bionic main' | sudo tee /etc/apt/sources.list.d/ansible.list",
            "sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367",
            "sudo apt update",
            "sudo apt -y install ansible apt-transport-https ca-certificates curl gnupg-agent software-properties-common python-pip git",
            "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -",
            "sudo add-apt-repository 'deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable'",
            "sudo apt -y update",
            "sudo apt -y install docker-ce docker-ce-cli containerd.io",
            "sudo curl -L https://github.com/docker/compose/releases/download/1.24.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose",
            "sudo chmod +x /usr/local/bin/docker-compose",
            "sudo pip install requests==2.14.2 docker docker-compose==1.24.1",
            "git clone --depth 50 https://github.com/ansible/awx.git",
            "sudo ansible-playbook -i /tmp/inventory $HOME/awx/installer/install.yml"
        ]
        connection {
            type = "ssh"
            user = "${var.REMOTE_USER}"
            host = "${self.network_interface.0.access_config.0.nat_ip}"
            private_key = "${file("~/.ssh/id_rsa")}"
        }
    }

}

output "ip" {
  value = "${google_compute_instance.awx.network_interface.0.access_config.0.nat_ip}"
}
