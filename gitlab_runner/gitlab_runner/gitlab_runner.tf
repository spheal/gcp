provider "google" {
    credentials = "${file("keys/key.json")}"
    project = "${var.PROJECT}"
    region = "${var.REGION}"
}
resource "google_compute_instance" "gitrunner" {
    name = "gitrunner"
    machine_type = "n1-standard-1"
    zone = "${var.ZONE}"
    labels = {
        goal = "ci_cd"
        service = "gitlabci"
    }
    boot_disk {
        auto_delete = true
        device_name = "gitrunner"
        mode = "READ_WRITE"
        initialize_params {
            size = 30
            type = "pd-standard"
            image = "coreos-cloud/coreos-stable"
        }
    }
    network_interface {
        network = "default"
        access_config {}
    }
    scheduling {
        preemptible = true
        automatic_restart = false
    }
    description = "VM for running GitLabCI runner"
    metadata = {
        ssh-keys = "${var.REMOTE_USER}:${file("~/.ssh/id_rsa.pub")}"
    }
    provisioner "remote-exec" {
        inline = [
            "sudo mkdir -p /opt/bin/",
            "sudo curl -L --output /opt/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64",
            "sudo chmod +x /opt/bin/gitlab-runner",
            "sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash -G docker",
            "sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner",
            "sudo gitlab-runner start",
            "sudo gitlab-runner register --non-interactive --docker-privileged  --name gitrunner --docker-image docker:latest --url https://gitlab.com/ --registration-token ${var.TOKEN} --executor docker --docker-volumes /var/run/docker.sock:/var/run/docker.sock:ro"
        ]
        connection {
            type = "ssh"
            user = "${var.REMOTE_USER}"
            host = "${self.network_interface.0.access_config.0.nat_ip}"
            private_key = "${file("~/.ssh/id_rsa")}"
        }
    }

}

output "ip" {
  value = "${google_compute_instance.gitrunner.network_interface.0.access_config.0.nat_ip}"
}
