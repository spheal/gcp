import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')


def test_gobetween_user(host):
    gu = host.user("gobetween")
    assert gu.shell == "/bin/false"
    assert gu.expiration_date is None


def test_gobetween_file(host):
    gf = host.file('/etc/gobetween/gobetween.toml')
    assert gf.exists
    assert gf.is_file
    assert gf.user == 'gobetween'


def test_gobetween_systemd(host):
    gs = host.file('/etc/systemd/system/gobetween.service')
    assert gs.exists
    assert gs.is_file


def test_gobetween_binary(host):
    gb = host.file('/usr/local/bin/gobetween')
    assert gb.exists
    assert gb.is_file


def test_gobetween_service(host):
    gsr = host.service('gobetween')
    assert gsr.is_running


def test_gobetween_port(host):
    gp = host.addr("localhost")
    assert gp.is_resolvable
    assert gp.port(8080).is_reachable
