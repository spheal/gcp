provider "google" {
    credentials = "${file("keys/key.json")}"
    project = "${var.PROJECT}"
    region = "${var.REGION}"
}
resource "google_compute_network" "sandbox" {
    name = "sandbox"
}

resource "google_compute_firewall" "web_ssh" {
    name = "allow-web-ssh"
    network = "${google_compute_network.sandbox.name}"
    allow {
        protocol = "tcp"
        ports = ["22","8080"]
    }
}

resource "google_compute_instance" "lb" {
    name = "lb"
    machine_type = "f1-micro"
    zone = "${var.ZONE}"
    labels = {
        goal = "lb"
    }
    boot_disk {
        auto_delete = true
        device_name = "lb"
        mode = "READ_WRITE"
        initialize_params {
            size = 10
            type = "pd-standard"
            image = "ubuntu-os-cloud/ubuntu-minimal-1804-lts"
        }
    }
    network_interface {
        network = "${google_compute_network.sandbox.name}"
        access_config {}
    }
    scheduling {
        preemptible = false
        automatic_restart = false
    }
    description = "VM for running LB"
    metadata = {
        ssh-keys = "${var.REMOTE_USER}:${file("~/.ssh/id_rsa.pub")}"
    }
}

resource "google_compute_instance" "app" {
    count = 2
    name = "app-${count.index}"
    machine_type = "f1-micro"
    zone = "${var.ZONE}"
    labels = {
        goal = "app-${count.index}"
    }
    boot_disk {
        auto_delete = true
        device_name = "app-${count.index}"
        mode = "READ_WRITE"
        initialize_params {
            size = 10
            type = "pd-standard"
            image = "ubuntu-os-cloud/ubuntu-minimal-1804-lts"
        }
    }
    network_interface {
        network = "${google_compute_network.sandbox.name}"
        access_config {}
    }
    scheduling {
        preemptible = true
        automatic_restart = false
    }
    description = "VM for running app"
    metadata = {
        ssh-keys = "${var.REMOTE_USER}:${file("~/.ssh/id_rsa.pub")}"
    }
}
# Generate inventory file
data "template_file" "inventory_data" {
    template = "${file("${path.module}/inventory.tpl")}"
    vars = {
        LB_IP = "${google_compute_instance.lb.network_interface.0.access_config.0.nat_ip}"
        APP_IP = "${join("\n",google_compute_instance.app.*.network_interface.0.access_config.0.nat_ip)}"
    }
    depends_on = ["google_compute_instance.app"]
}
resource "local_file" "inventory" {
    content = "${data.template_file.inventory_data.rendered}"
    filename = "${path.module}/ansible/inventory"

}
# Executing ansible playbook to deploy sandbox configuration.Sleep was used to initialize nodes,without it you can't connect to nodes.
resource "null_resource" "ansible-execution" {
  provisioner "local-exec" {
    command = "export ANSIBLE_HOST_KEY_CHECKING=False;sleep 60;ansible-playbook -i ${path.module}/ansible/inventory ${path.module}/ansible/main.yml"  
  }
  depends_on = ["local_file.inventory"]
}

output "ip" {
  value = "${google_compute_instance.lb.network_interface.0.access_config.0.nat_ip}"
}





